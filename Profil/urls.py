from django.urls import path
from Profil import views

urlpatterns = [
    path("", views.storyPPW, name='story3'),
    path("profil/", views.profil, name='Profil'),
    path("story/", views.story, name='story'),
    path("skill/", views.skill, name='skill'),
    path("education/", views.education, name='education'),
    path("contact/", views.contact, name='contact'),
    path("articel/", views.articel, name='articel'),
    path("challeng-form-isian/", views.chalenge, name='chalenge')
    ]